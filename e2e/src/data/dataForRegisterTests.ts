import {User, ErrorMessages} from "./Interfaces";

export const user: User = {
  emailExistingUser: "",
  emailNotExistingUser: "",
  emailNotValid: "",
  emailValid: "",

  passwordLessThanEight: "qwe134",
  passwordOnlyDigits: "12345678",
  passwordValid: "qwerty123456",
  passwordNotValid: "",

  dayValid: "15",
  dayNotValid: "40",
  dayEmpty: "",

  monthValid: "10",
  monthNotValid: "30",
  monthEmpty: "",

  yearValid: "2019",
  yearNotValid: "14311",
  yearEmpty: ""
};

export const errorMessages: ErrorMessages = {
  invalidDay: "Oops, that day doesn't look right.",
  invalidMonth: "Oops, that month doesn't look right.",
  invalidYear: "Oops, that year doesn't look right.",

  invalidDayAndMonth: "Oops, that day and month don't look right.",
  invalidDayAndYear: "Oops, that day and year don't look right.",
  invalidMonthAndYear: "Oops, that month and year don't look right.",
  invalidDayMonthYear: "Oops, that date doesn't look right."
};
