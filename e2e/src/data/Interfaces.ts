
export interface User {
  emailExistingUser: string,
  emailNotExistingUser: string,
  emailNotValid: string,
  emailValid: string,
  passwordLessThanEight: string,
  passwordOnlyDigits: string,
  passwordValid: string,
  passwordNotValid: string,
  dayValid: string,
  dayNotValid: string,
  dayEmpty: string,
  monthValid: string,
  monthNotValid: string,
  monthEmpty: string,
  yearValid: string,
  yearNotValid: string
  yearEmpty: string
}

export interface ErrorMessages {
  invalidDay: string,
  invalidMonth: string,
  invalidYear: string,

  invalidDayAndMonth: string,
  invalidDayAndYear: string,
  invalidMonthAndYear: string,
  invalidDayMonthYear: string
}
