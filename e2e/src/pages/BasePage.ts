import {browser, element, Locator, protractor} from "protractor";
import {Bottom} from "../component/Bottom";

export  class BasePage{

  constructor (){
  };

 protected bottom: Bottom = new Bottom();

 public async refresh(){
   await browser.get(browser.baseUrl);
 };
}
