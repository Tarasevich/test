import {BasePage} from "../pages/BasePage";
import {YearChoice} from "../component/registerpage/YearChoice";
import {BirthdayDateComponent} from "../component/registerpage/BirthdayDateComponent";
import {UserCredentialsPage} from "../component/registerpage/UserCredentialsPage";
import {browser} from "protractor";
import {errorMessages, user} from "../data/dataForRegisterTests";


describe('Register tests ',  () => {
  let basePage: BasePage = new BasePage();
  let yearChoice: YearChoice = new YearChoice();
  let birthdayDateComponent: BirthdayDateComponent = new BirthdayDateComponent();
  let userCredentialsPage: UserCredentialsPage = new UserCredentialsPage();

  beforeAll(async () => {
      await browser.get(browser.baseUrl);
  });

  beforeEach(async () => {
    await basePage.refresh();
  });

  it('Interfaces registration: Less than 13 years', async () => {
    expect(await yearChoice.isLessThanThirteenPresent()).toBe(true);
    expect(await yearChoice.isMoreThanThirteenPresent()).toBe(true);
    await yearChoice.chooseLessThanThirteen();
    expect(await browser.getCurrentUrl()).toContain("/sorry");
  });

  it('Interfaces registration: More than 13 years', async () => {
    await yearChoice.chooseMoreThanThirteen();
    expect(await birthdayDateComponent.isDayInputPresent()).toBe(true);
    expect(await birthdayDateComponent.isMonthInputPresent()).toBe(true);
    expect(await birthdayDateComponent.isYearInputPresent()).toBe(true);
    expect(await birthdayDateComponent.isSubmitPresent()).toBe(true);
  });

  it('Interfaces registration: Invalid day, valid month, valid year', async () => {
    await yearChoice.chooseMoreThanThirteen();
    await birthdayDateComponent.selectDay(user.dayNotValid);
    await birthdayDateComponent.selectMonth(user.monthValid);
    await birthdayDateComponent.selectYear(user.yearValid);
    expect(await birthdayDateComponent.getErrorMessage()).toBe(errorMessages.invalidDay);
  });

  it('Interfaces registration: Valid day, invalid month, valid year', async () => {
    await yearChoice.chooseMoreThanThirteen();
    await birthdayDateComponent.selectDay(user.dayValid);
    await birthdayDateComponent.selectMonth(user.monthNotValid);
    await birthdayDateComponent.selectYear(user.yearValid);
    expect(await birthdayDateComponent.getErrorMessage()).toBe(errorMessages.invalidMonth);
  });

  it('Interfaces registration: Valid day, valid month, invalid year', async () => {
    await yearChoice.chooseMoreThanThirteen();
    await birthdayDateComponent.selectDay(user.dayValid);
    await birthdayDateComponent.selectMonth(user.monthValid);
    await birthdayDateComponent.selectYear(user.yearNotValid);
    expect(await birthdayDateComponent.getErrorMessage()).toBe(errorMessages.invalidYear);
  });

  it('Interfaces registration: Valid day, invalid month, invalid year', async () => {
    await yearChoice.chooseMoreThanThirteen();
    await birthdayDateComponent.selectDay(user.dayValid);
    await birthdayDateComponent.selectMonth(user.monthNotValid);
    await birthdayDateComponent.selectYear(user.yearNotValid);
    expect(await birthdayDateComponent.getErrorMessage()).toBe(errorMessages.invalidMonthAndYear);
  });

  it('Interfaces registration: Invalid day, valid month, invalid year', async () => {
    await yearChoice.chooseMoreThanThirteen();
    await birthdayDateComponent.selectDay(user.dayNotValid);
    await birthdayDateComponent.selectMonth(user.monthValid);
    await birthdayDateComponent.selectYear(user.yearNotValid);
    expect(await birthdayDateComponent.getErrorMessage()).toBe(errorMessages.invalidDayAndYear);
  });

  it('Interfaces registration: Invalid day, invalid month, valid year', async () => {
    await yearChoice.chooseMoreThanThirteen();
    await birthdayDateComponent.selectDay(user.dayNotValid);
    await birthdayDateComponent.selectMonth(user.monthNotValid);
    await birthdayDateComponent.selectYear(user.yearValid);
    expect(await birthdayDateComponent.getErrorMessage()).toBe(errorMessages.invalidDayAndMonth);
  });

  it('Interfaces registration: Empty day, valid month, valid year', async () => {
    await yearChoice.chooseMoreThanThirteen();
    await birthdayDateComponent.selectDay(user.dayEmpty);
    await birthdayDateComponent.selectMonth(user.monthValid);
    await birthdayDateComponent.selectYear(user.yearValid);
    expect(await birthdayDateComponent.getErrorMessage()).toBe(errorMessages.invalidDay);
  });

  it('Interfaces registration: Valid day, empty month, valid year', async () => {
    await yearChoice.chooseMoreThanThirteen();
    await birthdayDateComponent.selectDay(user.dayValid);
    await birthdayDateComponent.selectMonth(user.monthEmpty);
    await birthdayDateComponent.selectYear(user.yearValid);
    expect(await birthdayDateComponent.getErrorMessage()).toBe(errorMessages.invalidMonth);
  });

  it('Interfaces registration: Valid day, valid month, empty year', async () => {
    await yearChoice.chooseMoreThanThirteen();
    await birthdayDateComponent.selectDay(user.dayValid);
    await birthdayDateComponent.selectMonth(user.monthValid);
    await birthdayDateComponent.selectYear(user.yearEmpty);
    expect(await birthdayDateComponent.getErrorMessage()).toBe(errorMessages.invalidYear);
  });

  it('Interfaces registration: Empty day, empty month, empty year', async () => {
    await yearChoice.chooseMoreThanThirteen();
    await birthdayDateComponent.selectDay(user.dayEmpty);
    await birthdayDateComponent.selectMonth(user.monthEmpty);
    await birthdayDateComponent.selectYear(user.yearEmpty);
    expect(await birthdayDateComponent.getErrorMessage()).toBe(errorMessages.invalidDayMonthYear);
  });

  it('Interfaces registration: Invalid day, invalid month, invalid year', async () => {
    await yearChoice.chooseMoreThanThirteen();
    await birthdayDateComponent.selectDay(user.dayNotValid);
    await birthdayDateComponent.selectMonth(user.monthNotValid);
    await birthdayDateComponent.selectYear(user.yearNotValid);
    expect(await birthdayDateComponent.getErrorMessage()).toBe(errorMessages.invalidMonthAndYear);
  });

  it('Interfaces registration: valid day, valid month, valid year', async () => {
    await yearChoice.chooseMoreThanThirteen();
    await birthdayDateComponent.selectDay(user.dayValid);
    await birthdayDateComponent.selectMonth(user.monthValid);
    await birthdayDateComponent.selectYear(user.yearValid);
    await birthdayDateComponent.submit();
    expect(userCredentialsPage.isEmailInputPresent).toBe(true);
    expect(userCredentialsPage.isPasswordPresent).toBe(true);
    expect(userCredentialsPage.isRegisterPresent).toBe(true);
  });








});
