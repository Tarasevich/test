import {BaseComponent} from "../BaseComponent";
import {TextField} from "../../control/TextField";
import {Button} from "../../control/Button";
import {by} from "protractor";


export class UserCredentialsPage extends BaseComponent{

  constructor(){
    super();
  }

  private email = new TextField(by.id("user-identifier-input"));
  private password = new TextField(by.id("password-input"));
  private register = new Button(by.id("submit-button"));

  public async setEmail(email: string){
    await this.email.setValue(email);
  }

  public async setPassword(password: string){
    await this.password.setValue(password);
  }

  public async submit(){
    await this.register.click();
  }

  public async isEmailInputPresent(){
    return await this.email.isElementPresent();
  }

  public async isPasswordPresent(){
    return await this.password.isElementPresent();
  }

  public async isRegisterPresent(){
    return await this.register.isElementPresent();
  }
}
