import {BaseComponent} from "../BaseComponent";
import {Button} from "../../control/Button";
import {by} from "protractor";


export class YearChoice extends BaseComponent {

  constructor(){
    super();
  }

  private lessThanThirteenBtn = new Button(by.partialLinkText("/guardian"));
  private moreThanThirteenBtn = new Button(by.partialLinkText("/age"));

  public async chooseLessThanThirteen(){
    await this.lessThanThirteenBtn.click();
  //  return await new BBCPage();
  }

  public async chooseMoreThanThirteen(){
    await this.moreThanThirteenBtn.click();
    //  return await new BirthdayDateComponent();
  }

  public async isLessThanThirteenPresent(){
    return await this.lessThanThirteenBtn.isElementPresent();
  }

  public async isMoreThanThirteenPresent(){
    return await this.moreThanThirteenBtn.isElementPresent();
  }
}
