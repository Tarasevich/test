import {BaseComponent} from "../BaseComponent";
import {TextField} from "../../control/TextField";
import {by} from "protractor";
import {Button} from "../../control/Button";
import {UserCredentialsPage} from "./UserCredentialsPage";
import {TextArea} from "../../control/TextArea";


export class BirthdayDateComponent extends BaseComponent {

  constructor() {
    super();
  }

  private dayInput = new TextField(by.id("day-input"));
  private monthInput = new TextField(by.id("month-input"));
  private yearInput = new TextField(by.id("year-input"));
  private submitBtn = new Button(by.id("submit-button"));
  private invalidDateMessage = new TextArea(by.css(".form-message__text span>span"));

  public async selectDay(day: string){
    await this.dayInput.setValue(day);
  }

  public async selectMonth(month: string){
    await this.monthInput.setValue(month);
  }

  public async selectYear(year: string){
    await this.yearInput.setValue(year);
  }

  public async submit(){
    await this.submitBtn.click();
    return await new UserCredentialsPage();
  }

  public async isDayInputPresent(){
    return await this.dayInput.isElementPresent();
  }

  public async isMonthInputPresent(){
    return await this.monthInput.isElementPresent();
  }

  public async isYearInputPresent(){
    return await this.yearInput.isElementPresent();
  }

  public async isSubmitPresent(){
    return await this.submitBtn.isElementPresent();
  }

  public async getErrorMessage(){
    return await this.invalidDateMessage.getText();
  }


}
