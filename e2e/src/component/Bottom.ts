import {Link} from "../control/Link";
import {BasePage} from "../pages/BasePage";
import {by} from "protractor";


export class Bottom extends BasePage{

  constructor(){
    super();
  };

  private getMoreAboutBBBCAccountsLink = new Link(by.partialLinkText("/account"));
  private termsOfUseLink = new Link(by.partialLinkText("/terms"));
  private aboutTheBBCLink = new Link(by.partialLinkText("/aboutthebbc"));
  private privacyPolicyLink = new Link(by.partialLinkText("/privacy"));
  private cookiesLink = new Link(by.partialLinkText("/coolies"));
  private accessibilityHelpLink = new Link(by.partialLinkText("/accessibility"));
  private parentalGuidanceLink = new Link(by.partialLinkText("/guidance"));
  private contactTheBBCLink = new Link(by.partialLinkText("/contact"));
  private getPersonalisedNewslettersLink = new Link(by.partialLinkText("/bbcnewsletter"));


  public async getMoreAboutBBBCAccounts(){
    await this.getMoreAboutBBBCAccountsLink.click();
  };

  public async openTermsOfUse(){
    await this.termsOfUseLink.click();
  };
  public async openAboutTheBBC(){
    await this.aboutTheBBCLink.click();
  };
  public async openPrivacyPolicy(){
    await this.privacyPolicyLink.click();
  };
  public async openCookies(){
    await this.cookiesLink.click();
  };
  public async openAccessibilityHelp(){
    await this.accessibilityHelpLink.click();
  };
  public async openParentalGuidance(){
    await this.parentalGuidanceLink.click();
  };
  public async openContactTheBBC(){
    await this.contactTheBBCLink.click();
  };
  public async getPersonalisedNewsletters(){
    await this.getPersonalisedNewslettersLink.click();
  };



  }
