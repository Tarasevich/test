import {browser, element, ElementArrayFinder, Locator} from "protractor";


export class BaseControl {

  protected element: ElementArrayFinder;

  constructor(locator: Locator) {
    this.element = element.all(locator);
  }

  public async getChild(locator: Locator){
    return await this.element.first().element(locator);
  }

  public async isDisplayed() {
    return await this.element.isDisplayed();
  };

  public async isElementPresent(){
    return await browser.isElementPresent(this.element);
  };

  public async getText(){
    return await this.element.getText();
  };

  public async getValue(){
    return await this.element.getAttribute("value");
  };

  public async getElementsCount(){
    return await this.element.count();
  }

  public async getNoteByIndex(index: number){
    return await this.element.get(index);
  }

  public async click(){
    return await this.element.click();
  }

}
