import {BaseControl} from "./BaseControl";
import {ElementFinder, Key, Locator} from "protractor";


export class TextField extends BaseControl {

  constructor(locator: Locator) {
    super(locator);
  };

  public async isDisabled(){
    return await this.element.getAttribute("disabled") === "disabled";
  };

  public async setValue(value: string){
    await this.element.clear();
    await this.element.sendKeys(value);
    await this.element.sendKeys(Key.TAB);
  };

  public async focus(){
    await this.element.click();
  }
  
}
