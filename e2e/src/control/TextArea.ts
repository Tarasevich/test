import {BaseControl} from "./BaseControl";
import {Locator} from "protractor";


export class TextArea extends BaseControl{

  constructor(locator: Locator){
    super(locator);
  }
}
