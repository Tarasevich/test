// @ts-check
// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts

const { SpecReporter } = require('jasmine-spec-reporter').SpecReporter;

/**
 * @type { import("protractor").Config }
 */
exports.config = {
  allScriptsTimeout: 11000,
  specs: [
    'spec/Register.ts'
  ],
  capabilities: {
    'browserName': 'chrome'
  },
  SELENIUM_PROMISE_MANAGER: false,
  directConnect: true,
  framework: 'jasmine',
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 30000,
    print: function () {
    }
  },
  onPrepare() {
    require('ts-node').register({
      project: require('path').join(__dirname, './tsconfig.json')
    });
    browser.driver.manage().window().setSize(1920, 1080);


    // browser.driver.manage().window().setSize(1366, 768);
    //jasmine.getEnv().addReporter(new SpecReporter());
    let allureReporter = require('jasmine-allure-reporter');
    jasmine.getEnv().addReporter(allureReporter);
    jasmine.getEnv().afterEach(function () {
      browser.takeScreenshot().then(function (png) {
        // create stream for writing the image
        let stream = createWriteStream("exception.png");
        // write the stream to local file
        stream.write(new Buffer(png, 'base64'));
        // close the stream
        stream.end();
      });
    });
  },
  rootElement: '[ng-app]',
  // Timeouts
  allScriptsTimeout: 600000,
  getPageTimeout: 600000,
  jasmineNodeOpts: {
    isVerbose: true,
    defaultTimeoutInterval: 600000,
    realtimeFailure: true, //grep: 'pattern',
    invertGrep: false,
    includeStackTrace: true,
  },
  baseUrl: 'https://twin.mps.gmbh/k1/linux/login.html',
  multiCapabilities: [{
    browserName: 'chrome',
    chromeOptions: {
      'args': ['--disable-cache', '--disable-application-cache', '--disable-offline-load-stale-cache', '--disk-cache-size=0', '--disable-extensions',
        '--v8-cache-options=off', '--disable-infobars', '--disable-web-security', '--ignore-certificate-errors'],
      'prefs': {
        'download': {
          'directory_upgrade': true,
          'prompt_for_download': false,
          'default_directory': __dirname + '/downloads/'
        },

        // disable chrome's annoying password manager
        "profile.password_manager_enabled": false,
        "credentials_enable_service": false,
        "password_manager_enabled": false
      }
    },
    shardTestFiles: true,
    maxInstances: 1
  }],

  suites: {
    basis: ['mps/e2e/src/spec/basis/Volltextsuche.spec.ts']
  }
};


// let tempConf = getConfiguration({
//   // the name 'twin-app-web' relates to the docker-container from docker-compose
//   baseUrl: 'https://twin.mps.gmbh/k1/linux/login.html', // use direct connect locally, makes the tests faster
//   directConnect: true
//
// });
// tempConf.multiCapabilities[0].maxInstances = 1;
// // chrome headless
// tempConf.multiCapabilities[0].chromeOptions.args =
//   ['--no-sandbox', '--headless', '--disable-gpu', '--disable-cache', '--disable-application-cache', '--disable-offline-load-stale-cache', '--disk-cache-size=0',
//     '--disable-extensions', '--v8-cache-options=off', '--disable-infobars', '--window-size=1366,768'];

